# Golden Path Java project

This project is meant to serve as an example of a good way to create Java applications for enterprise use.

## How to build

Run `gradle build` and wait until completion. Since TestContainers is used, the first run will be slower as various docker images are downloaded.

## How to run

Run docker-compose with the `local-docker-compose.yml` in src/test/resources. Then run `gradle boot:run` to start the application.

### Components

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.7.5/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.7.5/gradle-plugin/reference/html/#build-image)
* [Testcontainers Kafka Modules Reference Guide](https://www.testcontainers.org/modules/kafka/)
* [Testcontainers Postgres Module Reference Guide](https://www.testcontainers.org/modules/databases/postgres/)
* [Flyway Migration](https://docs.spring.io/spring-boot/docs/2.7.5/reference/htmlsingle/#howto.data-initialization.migration-tool.flyway)
* [Spring for Apache Kafka](https://docs.spring.io/spring-boot/docs/2.7.5/reference/htmlsingle/#messaging.kafka)
* [Spring Boot Actuator](https://docs.spring.io/spring-boot/docs/2.7.5/reference/htmlsingle/#actuator)
* [Testcontainers](https://www.testcontainers.org/)
* [Prometheus](https://docs.spring.io/spring-boot/docs/2.7.5/reference/htmlsingle/#actuator.metrics.export.prometheus)
* [Resilience4J](https://docs.spring.io/spring-cloud-circuitbreaker/docs/current/reference/html/#configuring-resilience4j-circuit-breakers)

