package se.rende.golden_path.queue;

public record StuffCreatedEvent(String externalId, String contents) {
}
