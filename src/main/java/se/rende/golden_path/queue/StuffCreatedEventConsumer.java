package se.rende.golden_path.queue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class StuffCreatedEventConsumer {
    private final StuffCreatedEventHandler eventHandler;
    private final ObjectMapper mapper;

    public StuffCreatedEventConsumer(StuffCreatedEventHandler service, ObjectMapper mapper) {
        this.eventHandler = service;
        this.mapper = mapper;
    }

    @KafkaListener(topics = "stuff-created", groupId = "stuff-created-listener-01", autoStartup = "true")
    public void consumeStuffCreatedEvent(String eventBody) throws JsonProcessingException {
        StuffCreatedEvent event = mapper.readValue(eventBody, StuffCreatedEvent.class);
        eventHandler.handleEvent(event);
    }
}
