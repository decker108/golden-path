package se.rende.golden_path.queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.lang.invoke.MethodHandles;

@Service
public class StuffCreatedEventHandler {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public void handleEvent(StuffCreatedEvent event) {
        log.info("Received StuffCreatedEvent with id {} and contents {}", event.externalId(), event.contents());
    }
}
