package se.rende.golden_path.queue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import se.rende.golden_path.model.Stuff;

import java.lang.invoke.MethodHandles;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

@Service
public class StuffCreatedEventProducer {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final KafkaTemplate<String, String> template;
    private final ObjectMapper mapper;

    public StuffCreatedEventProducer(KafkaTemplate<String, String> template, ObjectMapper mapper) {
        this.template = template;
        this.mapper = mapper;
    }

    public void produce(Stuff stuff) throws JsonProcessingException, InterruptedException, ExecutionException, TimeoutException {
        String key = stuff.externalId().toString();
        String data = mapper.writeValueAsString(Map.of("contents", stuff.contents(), "externalId", key));
        ListenableFuture<SendResult<String, String>> future = template.send("stuff-created", key, data);
        future.addCallback(sendRes -> {
            log.info("Successfully published message");
        }, ex -> {
            log.error("Failed to publish message", ex);
        });
    }
}
