package se.rende.golden_path.api;

public record CreateStuffRequest(String contents) {}
