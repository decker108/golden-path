package se.rende.golden_path.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.rende.golden_path.model.Stuff;
import se.rende.golden_path.queue.StuffCreatedEventProducer;
import se.rende.golden_path.repository.StuffRepository;

import java.lang.invoke.MethodHandles;
import java.net.URI;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

@RestController
public class StuffApi {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final StuffRepository repository;
    private final StuffCreatedEventProducer eventProducer;

    public StuffApi(StuffRepository repository, StuffCreatedEventProducer eventProducer) {
        this.repository = repository;
        this.eventProducer = eventProducer;
    }

    @GetMapping(path = "/stuff/{externalId}")
    public ResponseEntity<Stuff> getStuff(@PathVariable("externalId") String externalId) {
        Optional<Stuff> stuffOptional = repository.find(externalId);
        return stuffOptional.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping(path = "/stuff")
    public ResponseEntity<Void> createStuff(@RequestBody CreateStuffRequest createStuffRequest) {
        try {
            Stuff stuff = convertFromDto(createStuffRequest);
            repository.store(stuff);
            eventProducer.produce(stuff);
            return ResponseEntity.created(URI.create("/stuff/%s".formatted(stuff.externalId()))).build();
        } catch (JsonProcessingException | InterruptedException | ExecutionException | TimeoutException e) {
            log.error("Unexpected error handling createStuff request", e);
            return ResponseEntity.internalServerError().build();
        }
    }

    private static Stuff convertFromDto(CreateStuffRequest stuff) {
        return new Stuff(UUID.randomUUID(), stuff.contents());
    }
}
