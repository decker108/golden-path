package se.rende.golden_path;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoldenPathApplication {
	public static void main(String[] args) {
		SpringApplication.run(GoldenPathApplication.class, args);
	}
}
