package se.rende.golden_path.model;

import java.util.UUID;

public record Stuff(UUID externalId, String contents) {}
