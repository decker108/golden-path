package se.rende.golden_path;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.output.MigrateResult;
import org.jdbi.v3.core.Jdbi;
import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@Configuration
public class ApplicationConfig {

    @Bean
    public DataSource dataSource(Environment env) {
        return DataSourceBuilder.create()
                .username(env.getRequiredProperty("spring.datasource.username"))
                .password(env.getRequiredProperty("spring.datasource.password"))
                .url(env.getRequiredProperty("spring.datasource.url"))
                .driverClassName(env.getRequiredProperty("spring.datasource.driver-class-name"))
                .type(PGSimpleDataSource.class)
                .build();
    }

    @Bean
    public Jdbi jdbi(DataSource dataSource) {
        Jdbi jdbi = Jdbi.create(dataSource);
        Flyway flyway = new Flyway(Flyway.configure().dataSource(dataSource));
        MigrateResult result = flyway.migrate();
        if (!result.success) {
            throw new IllegalStateException("Flyway migration failure");
        }
        return jdbi;
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
