package se.rende.golden_path.repository;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.mapper.RowMapperFactory;
import org.springframework.stereotype.Repository;
import se.rende.golden_path.model.Stuff;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Repository
public class StuffRepository {
    private final Jdbi jdbi;

    public StuffRepository(Jdbi jdbi) {
        this.jdbi = jdbi;
        jdbi.registerRowMapper(RowMapperFactory.of(Stuff.class,
                (rs, ctx) -> new Stuff(UUID.fromString(rs.getString("external_id")),
                        rs.getString("contents"))));
    }

    public void store(Stuff stuff) {
        jdbi.useHandle(h -> h.createUpdate("INSERT INTO stuff(external_id, contents) VALUES(:externalId, :contents)")
                .bindMap(Map.of("externalId", stuff.externalId(), "contents", stuff.contents()))
                .execute());
    }

    public Optional<Stuff> find(String externalId) {
        return jdbi.withHandle(h -> h.select("SELECT external_id, contents FROM stuff WHERE external_id = :externalId")
                .bind("externalId", UUID.fromString(externalId))
                .mapTo(Stuff.class)
                .findFirst());
    }
}
