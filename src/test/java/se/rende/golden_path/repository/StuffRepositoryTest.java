package se.rende.golden_path.repository;

import org.flywaydb.core.Flyway;
import org.jdbi.v3.core.Jdbi;
import org.junit.ClassRule;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;
import se.rende.golden_path.model.Stuff;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class StuffRepositoryTest {

    @ClassRule
    public static PostgreSQLContainer<?> container = new PostgreSQLContainer<>(DockerImageName.parse("postgres"));
    private static StuffRepository stuffRepository;

    @BeforeAll
    static void beforeAll() {
        container.start();
        Jdbi jdbi = Jdbi.create(container.getJdbcUrl(), container.getUsername(), container.getPassword());
        new Flyway(Flyway.configure().dataSource(container.getJdbcUrl(), container.getUsername(), container.getPassword())).migrate();
        stuffRepository = new StuffRepository(jdbi);
    }

    @Test
    void shouldStoreAndFindStuffById() {
        Stuff stuff = new Stuff(UUID.randomUUID(), "test");
        stuffRepository.store(stuff);

        Optional<Stuff> optionalStuff = stuffRepository.find(stuff.externalId().toString());
        assertThat(optionalStuff).isPresent().contains(stuff);
    }
}