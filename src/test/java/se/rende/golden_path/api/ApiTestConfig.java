package se.rende.golden_path.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jdbi.v3.core.Jdbi;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;
import se.rende.golden_path.model.Stuff;
import se.rende.golden_path.queue.StuffCreatedEventProducer;
import se.rende.golden_path.repository.StuffRepository;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static org.mockito.Mockito.mock;
import static org.springframework.http.MediaType.APPLICATION_JSON;

//@Configuration
public class ApiTestConfig {

    public static final String EXAMPLE_STUFF_ID = "e442d60e-8d83-46dc-8e45-c8d1cbbe53f8";
    public static final String EXAMPLE_MISSING_STUFF_ID = "5f44f46c-96c2-4404-9119-0391e2a0d1cc";

    public static ResponseErrorHandler notFoundTolerantErrorHandler() {
        return new DefaultResponseErrorHandler() {
            @Override
            protected boolean hasError(@NotNull HttpStatus statusCode) {
                return statusCode.value() != 404 && super.hasError(statusCode);
            }
        };
    }

    @NotNull
    public static HttpHeaders headersWithJsonContentType() {
        var headers = new HttpHeaders();
        headers.setContentType(APPLICATION_JSON);
        return headers;
    }

//    @Primary
//    @Bean
//    public StuffRepository stuffRepository() {
//        Jdbi mockJdbi = mock(Jdbi.class);
//        return new StuffRepository(mockJdbi) {
//            @Override
//            public void store(Stuff stuff) {
//                // noop
//            }
//
//            @Override
//            public Optional<Stuff> find(String externalId) {
//                if (externalId.equalsIgnoreCase(EXAMPLE_MISSING_STUFF_ID)) {
//                    return Optional.empty();
//                }
//                return Optional.of(new Stuff(UUID.fromString(EXAMPLE_STUFF_ID), "test"));
//            }
//        };
//    }
//
//    @Primary
//    @Bean
//    public StuffCreatedEventProducer producer() {
//        return new StuffCreatedEventProducer(null) {
//            @Override
//            public void produce(Stuff stuff) throws JsonProcessingException, InterruptedException, ExecutionException, TimeoutException {
//                // noop
//            }
//        };
//    }
}
