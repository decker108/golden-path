package se.rende.golden_path.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;
import se.rende.golden_path.model.Stuff;
import se.rende.golden_path.queue.StuffCreatedEventProducer;
import se.rende.golden_path.repository.StuffRepository;

import java.net.URI;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static se.rende.golden_path.api.ApiTestConfig.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class StuffApiTest {

    @MockBean
    private StuffRepository mockStuffRepository;

    @MockBean
    private StuffCreatedEventProducer mockProducer;

    @LocalServerPort
    private int port;

    private static final ObjectMapper mapper = new ObjectMapper();
    private static final RestTemplate restTemplate = new RestTemplate();

    @BeforeAll
    static void beforeAll() {
        restTemplate.setErrorHandler(notFoundTolerantErrorHandler());
    }

    @Test
    void shouldReturnDataOnGetWithValidId() {
        when(mockStuffRepository.find(eq(EXAMPLE_STUFF_ID)))
                .thenReturn(Optional.of(new Stuff(UUID.fromString(EXAMPLE_STUFF_ID), "test")));
        URI url = new DefaultUriBuilderFactory().builder()
                .scheme("http")
                .host("localhost")
                .port(port)
                .pathSegment("stuff", EXAMPLE_STUFF_ID)
                .build();
        ResponseEntity<Stuff> response = restTemplate.getForEntity(url, Stuff.class);

        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(response.getHeaders().getContentType()).isEqualTo(APPLICATION_JSON);
        assertThat(response.getBody()).isEqualTo(new Stuff(UUID.fromString(EXAMPLE_STUFF_ID), "test"));
    }

    @Test
    void shouldReturn404ResponseOnGetWithValidButMissingId() {
        URI url = new DefaultUriBuilderFactory().builder()
                .scheme("http")
                .host("localhost")
                .port(port)
                .pathSegment("stuff", "{id}")
                .build(EXAMPLE_MISSING_STUFF_ID);
        ResponseEntity<Stuff> response = restTemplate.getForEntity(url, Stuff.class);

        assertThat(response.getStatusCodeValue()).isEqualTo(404);
    }

    @Test
    void shouldReturn200OnPostWithValidData() throws Exception {
        URI url = new DefaultUriBuilderFactory().builder()
                .scheme("http")
                .host("localhost")
                .port(port)
                .pathSegment("stuff")
                .build();
        String requestBody = mapper.writeValueAsString(new CreateStuffRequest("other-test"));
        RequestEntity<String> request = new RequestEntity<>(requestBody, headersWithJsonContentType(), HttpMethod.POST, url);
        ResponseEntity<Void> response = restTemplate.exchange(request, Void.class);

        assertThat(response.getStatusCodeValue()).isEqualTo(201);
        assertThat(response.getHeaders().getLocation().toString()).contains("/stuff/");
    }
}