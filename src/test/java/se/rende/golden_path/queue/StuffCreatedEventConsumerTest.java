package se.rende.golden_path.queue;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.jetbrains.annotations.NotNull;
import org.junit.ClassRule;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.springframework.util.concurrent.ListenableFuture;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;
import se.rende.golden_path.GoldenPathApplication;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = {
        GoldenPathApplication.class})
@ContextConfiguration(initializers = {StuffCreatedEventConsumerTest.PropertyOverrideContextInitializer.class})
class StuffCreatedEventConsumerTest {

    @ClassRule
    public static KafkaContainer container = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:6.2.1"))
            .withEmbeddedZookeeper();

    private static final ObjectMapper mapper = new ObjectMapper();

    @MockBean
    private StuffCreatedEventHandler serviceMock;

    @BeforeAll
    static void beforeAll() {
        container.start();
    }

    @AfterAll
    static void afterAll() {
        container.stop();
    }

    @Test
    void shouldConsumeValidEvent() throws Exception {
        KafkaTemplate<String, String> producer = makeTestProducer(container);
        String externalId = UUID.randomUUID().toString();
        String eventBody = mapper.writeValueAsString(Map.of("externalId", externalId, "contents", "test"));

        ListenableFuture<SendResult<String, String>> future = producer.send("stuff-created", externalId, eventBody);
        future.get(3, TimeUnit.SECONDS);

        verify(serviceMock, timeout(1000)).handleEvent(eq(new StuffCreatedEvent(externalId, "test")));
    }

    private KafkaTemplate<String, String> makeTestProducer(KafkaContainer container) {
        return new KafkaTemplate<>(() -> new KafkaProducer<>(Map.of(
                CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, container.getBootstrapServers(),
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, org.apache.kafka.common.serialization.StringSerializer.class,
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, org.apache.kafka.common.serialization.StringSerializer.class
        )));
    }

    public static class PropertyOverrideContextInitializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(@NotNull ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
                    configurableApplicationContext, "spring.kafka.bootstrap-servers=" + container.getBootstrapServers());
        }
    }
}