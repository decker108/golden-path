package se.rende.golden_path.queue;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.jetbrains.annotations.NotNull;
import org.junit.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.core.KafkaTemplate;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;
import se.rende.golden_path.model.Stuff;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class StuffCreatedEventProducerTest {

    @Rule
    public KafkaContainer container = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:6.2.1"))
            .withEmbeddedZookeeper();

    private static final ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        container.start();
    }

    @AfterEach
    void tearDown() {
        container.stop();
    }

    @Test
    void shouldProduceValidEvent() throws Exception {
        StuffCreatedEventProducer producer = new StuffCreatedEventProducer(createKafkaTemplate(container), mapper);
        Stuff stuff = new Stuff(UUID.randomUUID(), "test");
        KafkaConsumer<String, String> testConsumer = createTestConsumer(container);
        testConsumer.subscribe(List.of("stuff-created"));

        producer.produce(stuff);

        ConsumerRecords<String, String> response = testConsumer.poll(Duration.ofSeconds(3));
        Iterable<ConsumerRecord<String, String>> records = response.records("stuff-created");
        String eventBody = records.iterator().next().value();
        assertThat(mapper.readValue(eventBody, StuffCreatedEvent.class)).isEqualTo(new StuffCreatedEvent(stuff.externalId().toString(), stuff.contents()));
    }

    @NotNull
    private static KafkaConsumer<String, String> createTestConsumer(KafkaContainer container) {
        Map<String, Object> configs = Map.of(
                CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, container.getBootstrapServers(),
                ConsumerConfig.GROUP_ID_CONFIG, "test-consumer-group",
                ConsumerConfig.CLIENT_ID_CONFIG, "test-consumer",
                ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return new KafkaConsumer<>(configs, new StringDeserializer(), new StringDeserializer());
    }

    @NotNull
    private static KafkaTemplate<String, String> createKafkaTemplate(KafkaContainer container) {
        return new KafkaTemplate<>(() -> new KafkaProducer<>(Map.of(
                CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, container.getBootstrapServers(),
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, org.apache.kafka.common.serialization.StringSerializer.class,
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, org.apache.kafka.common.serialization.StringSerializer.class
        )));
    }
}